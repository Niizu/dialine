function toCase(str, choice) { // склонение существительных
    var strPub = { // правила для окончаний
            "а": ["ы", "е", "у", "ой", "е"],
            "(ш/ж/к/ч)а": ["%и", "%е", "%у", "%ой", "%е"],
            "б/в/м/г/д/л/ж/з/к/н/п/т/ф/ч/ц/щ/р/х": ["%а", "%у", "%а", "%ом", "%е"],
            "и": ["ей", "ям", "%", "ями", "ях"],
            "ый": ["ого", "ому", "%", "ым", "ом"],
            "й": ["я", "ю", "я", "ем", "е"],
            "о": ["а", "у", "%", "ом", "е"],
            "с/ш": ["%а", "%у", "%", "%ом", "%е"],
            "ы": ["ов", "ам", "%", "ами", "ах"],
            "ь": ["я", "ю", "я", "ем", "е"],
            "уль": ["ули", "уле", "улю", "улей", "уле"],
            "(ч/ш/д/т)ь": ["%и", "%и", "%ь", "%ью", "%и"],
            "я": ["и", "е", "ю", "ей", "е"]
        },
        cases = { // номера для падежей, не считая Именительный
            "р": 0,
            "д": 1,
            "в": 2,
            "т": 3,
            "п": 4
        },
        exs = { // исключения, сколько символов забирать с конца
            "ц": 2,
            "ок": 2
        },
        lastIndex,reformedStr,forLong,splitted,groupped,forPseudo;
    for(var i in strPub){
        if(i.length > 1 && str.slice(-i.length) == i){ // для окончаний, длиной >1
            lastIndex = i;
            reformedStr = str.slice(0, -lastIndex.length);
            break;
        }
        else if(/[\(\)]+/g.test(i)){ // фича: группировка окончаний
            i.replace(/\(([^\(\)]+)\)([^\(\)]+)?/g, function(a, b, c){
                splitted = b.split("/");
                for(var o = 0; o < splitted.length; o++){
                    groupped = splitted[o] + c;
                    strPub[groupped] = strPub[i];
                    if(str.slice(-groupped.length) == groupped){
                        for(var x = 0, eachSplited = strPub[groupped];x < eachSplited.length; x++){
                            eachSplited[x] = eachSplited[x].replace("%", splitted[o]);
                        }
                        reformedStr = str.slice(0, -groupped.length);
                        forPseudo = groupped;
                    }
                }
            })
        }
        else{ // дефолт
            lastIndex = str.slice(-1);
            reformedStr = str.slice(0, -(forPseudo || lastIndex).length);
        }
        if(/\//.test(i) && !(/[\(\)]+/g.test(i)) && new RegExp(lastIndex).test(i))forLong = i; // группированные окончания, разделающиеся слешем
        for(var o in exs){ // поиск исключений
            if(str.slice(-o.length) == o)reformedStr = str.slice(0, -exs[o]);
        }
    }
    return reformedStr + strPub[(forPseudo || forLong || lastIndex)][cases[choice]].replace("%", lastIndex)
}



$(document).ready(function(){
    var city_name_block = $('.section1__country_name');
    if (YMaps.location) { //определение города
        var country_name = YMaps.location.country;
        var city_name = YMaps.location.city;
        if (country_name != 'Россия') {
            $(function(){ //маска телефона для не России
                $.mask.definitions['~']='[ 1234567890+_]';
                $("input[name='phone']").mask("+~~~(999) 999-9999");
            });
            city_name_block.text('в странах СНГ');
        } else {
            $(function(){ //маска телефона для России
                $.mask.definitions['~']='[ 1234567890+_]';
                $("input[name='phone']").mask("+7(999) 999-9999");
            });
            city_name =  toCase(city_name,'п');
            city_name_block.text('в '+city_name);
        }
    }else{
        city_name_block.text('в мире');
    }

    d=new Date(); // даты +2 дня
    p=new Date(d.getTime()-(0));
    monthA=['01','02','03','04','05','06','07','08','09','10','11','12'];
    var date_to = (p.getDate()+2)+'.'+monthA[p.getMonth()]+'.'+p.getFullYear();
    $('.x_price_previous').text(date_to);

    $('.mouse_moved').mouseleave(function(){
        var displayP1 = $(".popup-phone").css('display');
        var displayP2 = $(".popup-callback").css('display');
        if ( displayP1 == 'none' && displayP2 == 'none'){
            $('.modal_mouse_out').show();
        }
        else{
            $('.modal_mouse_out').hide();
        }
    });
     $('.close-mouse').on('click', function(event) { //модальное окно
        event.preventDefault();
        $(".popup-callback").hide();
        $(".popup-phone").hide();
        $(".modal_mouse_out").hide();
    });

    $('.popup-mouseleave .close-modal,.popup-mouseleave .close-img').on('click', function(event) {
        event.preventDefault();
        $('.popup-mouseleave').addClass('hide');
    });
    $('.close-phone').click(function(){
        $('.popup-phone').hide();
    });

});
var storage_count =  localStorage.getItem('count'); //значение из хранилища
if(storage_count == ''){ // если пусто, то установить со страницы
    var count_items = parseInt($('.section7 .lastpack').text());
    localStorage.setItem('count', count_items);
}else{
    var count_block = $('.lastpack'); //если нет, то отсчитывать до 7 с интервалом от 10 до 15 сек
    function tick() {
           var interval = Math.floor(Math.random() * (15 - 10 + 1)) + 10;
           storage_count--;
           localStorage.setItem('count', storage_count);
           if (storage_count>=7) {
               count_block.text(storage_count);
           }else{
               storage_count = 60;
           }
           setTimeout(tick, interval * 1000);
       }
       tick();
}
function getParameterByName(name, url) { // парсинг url
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
var pathname = window.location.search;
callback = getParameterByName('callback');
if(callback){ // если в url есть параметр с именем callback
    alert('Есть параметр callback, со значением:'+ callback);
}
